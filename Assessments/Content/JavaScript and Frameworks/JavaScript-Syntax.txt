==========
Objective: Identify built-in data types
==========

[[
What two are standard JavaScript types?  (Choose two)
]]
1: <code>Void</code>
*2: <code>Date</code>
*3: <code>Error</code>
4: <code>Float</code>
5: <code>Undefined</code>


==========
Objective: Explain variable scoping rules including the global namespace
==========

[[
Analyze the following, non-strict JavaScript code:

<pre>
var sum;
function average(array) {
   var sum = 0, count = 0;
   for (idx=0; idx &LT; array.length; idx++) {
      sum += array[idx];
      count++;
   }
   return sum / count;
}</pre>

Which three variables are local to the <code>average</code> function?  (Choose three)
]]
*1: <code>sum</code>
2: <code>idx</code>
*3: <code>array</code>
*4: <code>count</code>
5: <code>average</code>
[[TEST: what about strict mode]]


==========
Objective: Identify the proper syntax of creating an object and accessing/manipulating elements
==========

[[
Which three code snippets create a JavaScript object with a variable named
<code>foo</code> and a value of <code>47</code>?  (Choose three)
]]
*1: <pre>var obj = {};
obj.foo = 47;</pre>
*2: <pre>var obj = { foo : 47 };</pre>
*3: <pre>var obj = new Object();
obj['foo'] = 47;</pre>
4:  <pre>var obj = new Object();
obj.setFoo(47);</pre>
5:  <pre>var obj = new Object();
obj.foo(47);</pre>
6:  <pre>var obj = ({}.foo = 47);</pre>
