==========
Objective: Identify the purpose of different types of JOINs
==========

[[
Which type of join is needed when you wish to include rows that
do <em>not</em> have matching values?
]]
1: Equi-join
2: Inner join
*3: Outer join
4: Natural join
5: Cartesian product


==========
Objective: Write WHERE clauses to constrained the result set using
 a variety of conditional operators
==========

[[
You are building a standard SQL query to search for products with a
partial string match.  The search needs to scan both SKU code and product
name.

Which conditional clause would satisfy these requirements?
]]
1: <code>sku ~= searchText OR name ~= searchText</code>
2: <code>sku LIKE searchText AND name LIKE searchText</code>
*3: <code>sku LIKE '*' || searchText || '*' OR name LIKE '*' || searchText || '*'</code>
4: <code>sku LIKE '?' || searchText || '?' AND name LIKE '?' || searchText || '?'</code>

[[NEW ITEM: one that doesn't use wildcards]]

[[
You are building a standard SQL query to search for products with an
exact string match.  The search needs to scan either SKU code or product name.

Which conditional clause would satisfy these requirements?
]]
*1:  <code>sku = searchText OR name = searchText</code>
2:  <code>sku = searchText AND name = searchText</code>
3:  <code>sku == searchText OR name == searchText</code>
4:  <code>sku == searchText AND name == searchText</code>


==========
Objective: Write the code to perform data aggregations
==========

[[
Given a database table defined like this:

<pre><strong>User</strong>
username: string
address: string
city: string
stateCode: string
age: integer</pre>

Which query determines the minimum age of users by state?
]]
1: <pre>SELECT stateCode, MINIMUM(age)
FROM User
GROUP BY stateCode</pre>
2: <pre>SELECT stateCode, MINIMUM(age)
FROM User
AGGREGATE ON age</pre>
*3: <pre>SELECT stateCode, MIN(age)
FROM User
GROUP BY stateCode</pre>
4: <pre>SELECT stateCode, MIN(age)
FROM User
AGGREGATE ON age</pre>
