==========
Objective: Apply the Code to an interface principle.
==========

[[
You need to write a method that performs a database query and returns a collection of <code>Employee</code> records.
<br/><br/>
Which method signature adheres to the "<em>Code to an interface</em>" principle?
]]
1: <code>public Employee[] searchEmployees(HashMap&LT;String,Object&GT; filters)</code>
*2: <code>public List&LT;Employee&GT; searchEmployees(Map&LT;String,Object&GT; filters)</code>
3: <code>public List&LT;Employee&GT; searchEmployees(HashMap&LT;String,Object&GT; filters)</code>
4: <code>public ArrayList&LT;Employee&GT; searchEmployees(HashMap&LT;String,Object&GT; filters)</code>


==========
Objective: Identify one or more refactorings that can mitigate non-DRY code.
==========

[[
"<em>Don't repeat yourself</em>" (DRY) is a fundamental coding principle.
Code duplication is the root cause of violations in the DRY principle.
<br/><br/>
Which two refactorings can you apply to make your code more DRY?  (Choose two)
]]
1: Inline Class
*2: Extract Class
*3: Extract Method
4: Replace Record with Data Class
