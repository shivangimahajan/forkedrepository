(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 SQL
19 Database Design

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):
(Course Site):
(Course Name):
(Course URL):
(Discipline):
(ENDIGNORE)

(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): A foreign key is which of the following?
(A): Any attribute
(B): The same thing as a primary key
(C): An attribute that serves as the primary key of another relation
(D): An attribute that serves no purpose
(Correct): C
(Points): 1
(CF): Foreign keys link data in one table to data in other tables.
(WF): Foreign keys link data in one table to data in other tables.
(STARTIGNORE)
(Hint): 
(Subject):Database Design
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): A(n) ____________ is a collection of structures with appropriate authorization and access defined.
(A): Table
(B): Class
(C): Database
(D): Query
(E): Entity
(Correct): C
(Points): 1
(CF): Database is defined as a structured set of data held in a computer, especially one that is accessible in various ways.
(WF): Database is defined as a structured set of data held in a computer, especially one that is accessible in various ways.
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): A view is which of the following?
(A): A virtual table that can be accessed via SQL commands
(B): A virtual table that cannot be accessed via SQL commands
(C): A base table that can be accessed via SQL commands
(D): A base table that cannot be accessed via SQL commands
(Correct): A
(Points): 1
(CF): When querying a view it can be treated the same as a table.
(WF): Views can be used with complex queries to precompile the results on the database server for faster access.
(STARTIGNORE)
(Hint): 
(Subject): Database Design
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is the correct order of keywords for SQL SELECT statements?
(A): FROM, WHERE, SELECT
(B): WHERE, FROM,SELECT
(C): SELECT,WHERE,FROM
(D): SELECT, FROM, WHERE
(Correct): D
(Points): 1
(CF): Select statements will error if they are not in the correct order.  Data you want returned; from what tables; and criteria to limit the results.
(WF): Select statements will error if they are not in the correct order.  Data you want returned; from what tables; and criteria to limit the results.
(STARTIGNORE)
(Hint): 
(Subject): SQL
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): In a SQL SELECT statement querying a single table, according to the SQL-92 standard the asterisk (*) means that:
(A): all records with even partial criteria met are to be returned.
(B): all columns of the table are to be returned.
(C): all records meeting the full criteria are to be returned.
(D): None of the above is correct.
(Correct): B
(Points): 1
(CF): In the select portion of the statement the wildcard will return all rows for a query.
(WF): If multiple tables are queried the combined rows for all tables will be returned.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): The HAVING clause does which of the following?
(A): Acts like a WHERE clause but is used for groups rather than rows.
(B): Acts like a WHERE clause but is used for rows rather than columns.
(C): Acts like a WHERE clause but is used for columns rather than groups.
(D): Acts EXACTLY like a WHERE clause.
(Correct): A
(Points): 1
(CF): HAVING can be used along with the GROUP BY clause to limit the amount of data.
(WF): HAVING can be used along with the GROUP BY clause to limit the amount of data.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): To remove duplicate rows from the results of an SQL SELECT statement, the ________ qualifier specified must be included.
(A): ONLY
(B): UNIQUE
(C): DISTINCT
(D): SINGLE
(Correct): C
(Points): 1
(CF): Using DISTINCT in the SELECT clause will only show records that are unique.
(WF): Using DISTINCT in the SELECT clause will only show records that are unique.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which one of the following sorts rows in SQL?
(A): SORT BY
(B): ALIGN BY
(C): ORDER BY
(D): GROUP BY
(Correct): C
(Points): 1
(CF): ORDER BY is used after the WHERE clause to sort the data returned from the query.
(WF): ORDER BY is used after the WHERE clause to sort the data returned from the query.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): SQL can be used to:
(A): create database structures only.
(B): query database data only.
(C): modify database data only.
(D): All of the above can be done by SQL. 
(Correct): D
(Points): 1
(CF): SQL can be used to add, update and query data that is stored in the database.
(WF): SQL can be used to add, update and query data that is stored in the database.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): The SQL WHERE clause:
(A): limits the column data that is returned.
(B): limits the row data that is returned.
(C): Both A and B are correct.
(D): Neither A nor B are correct.
(Correct): B
(Points): 1
(CF): Column data can be limited in the SELECT clause by specifying the table columns you would like to see.
(WF): Column data can be limited in the SELECT clause by specifying the table columns you would like to see.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What type of join is needed when you wish to include rows that do not have matching values?
(A): Equi-join
(B): Outer join
(C): Natural join
(D): Inner join
(E): Any of the above
(Correct): B
(Points): 1
(CF): Outer joins show data for the entities regardless of matching key values.
(WF): Outer joins show data for the entities regardless of matching key values.
(STARTIGNORE)
(Hint): 
(Subject): SQL
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What operator performs pattern matching?
(A): IS NULL operator
(B): ASSIGNMENT operator
(C): LIKE operator
(D): NOT operator
(Correct): C
(Points): 1
(CF): The LIKE operator can be used with wilcards to search text (ex. WHERE tst_clmn LIKE 'test%').
(WF): The LIKE operator can be used with wilcards to search text (ex. WHERE tst_clmn LIKE 'test%').
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): The command to remove rows from a table 'CUSTOMER' is:
(A): REMOVE FROM CUSTOMER ...
(B): DROP FROM CUSTOMER ...
(C): DELETE FROM CUSTOMER WHERE ...
(D): UPDATE FROM CUSTOMER ...
(Correct): C
(Points): 1
(CF): The DELETE statement is used to delete rows in a table.
(WF): The DELETE statement is used to delete rows in a table.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): A recursive relationship is a relationship between an entity and ________ .
(A): itself
(B): a subtype entity
(C): an archetype entity
(D): an instance entity
(Correct): A
(Points): 1
(CF): This is also known as a parent/child relationship.
(WF): This is also known as a parent/child relationship.
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Which of the following indicates the minimum number of entities that must be involved in a relationship?
(A): Minimum cardinality
(B): Maximum cardinality
(C): ERD
(D): Greater Entity Count (GEC)
(Correct): A
(Points): 1
(CF): Minimum cardinality is usually 0 or 1.
(WF): Minimum cardinality is usually 0 or 1.
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category):
(Grade style): 0
(Random answers): 1
(Question): What is the best data type definition for Oracle when a field is alphanumeric and has a length that can vary?
(A): CHAR
(B): LONG
(C): VARCHAR2
(D): NUMBER
(Correct):  C
(Points): 1
(CF): Variable-length character string having maximum length size bytes or characters. Maximum size is 4000 bytes or characters, and minimum is 1 byte or 1 character. You must specify size for VARCHAR2. 
(WF): Variable-length character string having maximum length size bytes or characters. Maximum size is 4000 bytes or characters, and minimum is 1 byte or 1 character. You must specify size for VARCHAR2. 
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Intermediate
(Applicability): Oracle
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): A multidimensional database model is used most often in which of the following models?
(A): Data warehouse
(B): Relational
(C): Hierarchical
(D): Network
(Correct): A
(Points): 1
(CF): Dimension tables are tied to fact tables and used for storing large amounts of data.
(WF): Dimension tables are tied to fact tables and used for storing large amounts of data.
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): The SQL command to create a table is:
(A): MAKE TABLE
(B): ALTER TABLE
(C): DEFINE TABLE
(D): CREATE TABLE
(Correct): D
(Points): 1
(CF): To create a new table in your schema, you must have the CREATE TABLE system privilege.
(WF): To create a new table in your schema, you must have the CREATE TABLE system privilege.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): A ________ is a stored program that is attached to a table or a view.
(A): pseudofile
(B): embedded SELECT statement
(C): trigger
(D): stored procedure
(E): None of the above is correct.
(Correct): C
(Points): 1
(CF): Triggers are procedures that are stored in the database and are implicitly run, or fired, when something happens.
(WF): Triggers are procedures that are stored in the database and are implicitly run, or fired, when something happens.
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a type of SQL constraint?
(A): PRIMARY KEY
(B): FOREIGN KEY
(C): ALTERNATE KEY
(D): UNIQUE
(Correct): C
(Points): 1
(CF): NOT NULL, unique, primary, foreign, check and referencial constraint are the 6 types of constraints.
(WF): NOT NULL, unique, primary, foreign, check and referencial constraint are the 6 types of constraints.
(STARTIGNORE)
(Hint):
(Subject): Databse Design
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): When using the SQL INSERT statement:
(A): rows can be modified according to criteria only.
(B): rows cannot be copied in mass from one table to another only.
(C): rows can be inserted into a table one at a time only.
(D): rows can either be inserted into a table one at a time or in groups.
(Correct): D
(Points): 1
(CF): Formatting of the data in the insert statement must match the table format.
(WF): Formatting of the data in the insert statement must match the table format.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Which of the following is a SQL trigger supported by Oracle?
(A): BEFORE
(B): AFTER
(C): INSTEAD OF
(D): All of the above.
(Correct): D
(Points): 1
(CF): Triggers can be setup to execute before, after or instead of a process on a table.
(WF): Triggers can be setup to execute before, after or instead of a process on a table.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Advanced
(Applicability): Oracle
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): The SQL ALTER statement can be used to:
(A): change the table structure.
(B): change the table data.
(C): add rows to the table.
(D): delete rows from the table.
(Correct): A
(Points): 1
(CF): The ALTER TABLE statement is used to add, delete, or modify columns in an existing table.
(WF): The ALTER TABLE statement is used to add, delete, or modify columns in an existing table.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): Views constructed from SQL SELECT statements that conform to the SQL-92 standard may not contain:
(A): GROUP BY.
(B): WHERE.
(C): ORDER BY.
(D): FROM.
(Correct): C
(Points): 1
(CF): Views are essentially virtual tables, the ORDER BY would be used in the SELECT statement to view the table in that table.
(WF): Views are essentially virtual tables, the ORDER BY would be used in the SELECT statement to view the table in that table.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): The following SQL is which type of join: SELECT CUSTOMER_T. CUSTOMER_ID, ORDER_T. CUSTOMER_ID, NAME, ORDER_ID FROM CUSTOMER_T,ORDER_T;
(A): Equi-join
(B): Natural join
(C): Outer join
(D): Cartesian join
(Correct): D
(Points): 1
(CF): A Cartesian join or Cartesian product is a join of every row of one table to every row of another table.
(WF): A Cartesian join or Cartesian product is a join of every row of one table to every row of another table.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Advanced
(Applicability): Genera;
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): A UNION query is which of the following?
(A): Combines the output from no more than two queries and must include the same number of columns.
(B): Combines the output from no more than two queries and does not include the same number of columns.
(C): Combines the output from multiple queries and must include the same number of columns.
(D): Combines the output from multiple queries and does not include the same number of columns.
(Correct): C
(Points): 1
(CF): A UNION can be used to combine data from multiple SELECT statements provided the output format for each is the same.
(WF): A UNION can be used to combine data from multiple SELECT statements provided the output format for each is the same.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 19
(Grade style): 0
(Random answers): 1
(Question): A CASE SQL statement is which of the following?
(A): A way to establish an IF-THEN-ELSE in SQL.
(B): A way to establish a loop in SQL.
(C): A way to establish a data definition in SQL.
(D): All of the above.
(Correct): A
(Points): 1
(CF): Evaluates a list of conditions and returns one of multiple possible result expressions.
(WF): Evaluates a list of conditions and returns one of multiple possible result expressions.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): In enterprise data modeling, which is incorrect?
(A): You review current systems.
(B): You implement the new database.
(C): You describe the data needed at a very high level of abstraction.
(D): You plan one or more database development projects.
(Correct): B
(Points): 1
(CF): An Enterprise Data Model (EDM) represents a single integrated definition of data, unbiased of any system or application.
(WF): An Enterprise Data Model (EDM) represents a single integrated definition of data, unbiased of any system or application.
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): The three-schema components include all, but:
(A): internal schema.
(B): conceptual schema.
(C): programming schema.
(D): external schema.
(Correct): C
(Points): 1
(CF): The goal of Three-Schema architecture is to separate the user applications and physical database.
(WF): The goal of Three-Schema architecture is to separate the user applications and physical database.
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): A secondary key is which of the following?
(A): Nonunique key
(B): Primary key
(C): Useful for denormalization decisions
(D): Determines the tablespace required
(Correct): A
(Points): 1
(CF): Secondary keys are used to view records in an order that is different from the order defined by the primary key fields.
(WF): Secondary keys are used to view records in an order that is different from the order defined by the primary key fields.
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Sequential retrieval on a primary key for sequential file storage has which of the following features?
(A): Very fast
(B): Impractical
(C): Slow
(D): Large system memory usage
(Correct): A
(Points): 1
(CF): Primary keys are automatically indexed.
(WF): Primary keys are automatically indexed.
(STARTIGNORE)
(Hint):
(Subject): Database Design
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)