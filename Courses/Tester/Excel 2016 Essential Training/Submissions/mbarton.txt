(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
30 Excel

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton	
(Course Site): Lynda
(Course Name): Excel 2016 Essential Training
(Course URL): https://www.lynda.com/Excel-tutorials/Excel-2016-Essential-Training/376985-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 30 
(Grade style): 0
(Random answers): 1
(Question): You click once a cell that has the word "cat" in it and begin typing the word "dog", and then hit Return on your keyboard. The cell
(A): has "cat" on top and "dog" on the bottom
(B): now says "catdog"
(C): now says "dog"
(D): says cat, and the cell below it says "dog"
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 30
(Grade style): 0
(Random answers): 1
(Question): What is true of formatting cells to a particular category, such as Accounting or Currency?
(A): Formatting tends to clutter data and should be done sparingly
(B): It adjusts the display of the data, but not the data itself
(C): You have 4 formatting categories to choose from: General, Number, Currency, and Accounting
(D): Various special characters are added to the cell data, depending on your choice
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 30
(Grade style): 0
(Random answers): 1
(Question): When moving or copying data from one cell to another, in order to maintain the formulas of the original cell you must _____.
(A): Re-write the formulas after copying the data
(B): Simply complete the action; Excel maintains the formulas for you
(C): First right-click the data to lock the formulas
(D): Lock the formulas through the button on the Data ribbon tab
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 30
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not type of Sparkline (mini chart)?
(A): Line
(B): Column
(C): Win/Loss
(D): Scatter 
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 30
(Grade style): 0
(Random answers): 1
(Question): A fast way to make a copy of a sheet in the same workbook is to drag the sheet tab with the _____ key held down.
(A): Tab
(B): Shift
(C): Cmd
(D): Ctrl
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 30
(Grade style): 0
(Random answers): 1
(Question): When using the VLOOKUP function, _____ means use "exact match".
(A): VALUE
(B): FALSE
(C): Index_Number
(D): EXACT
(Correct): B
(Points): 1
(CF): Setting the third parameter of the function to FALSE means Excel will only match on values in your lookup tables exactly.
(WF): Setting the third parameter of the function to FALSE means Excel will only match on values in your lookup tables exactly.
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Intermediate
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 30
(Grade style): 0
(Random answers): 1
(Question): If you lock a cell and then protect the worksheet, _____.
(A): the cell cannot be edited by others
(B): a formula in that cell will not react to changes
(C): others will be able to edit the cell
(D): the cell will be hidden from view
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 30
(Grade style): 0
(Random answers): 1
(Question): To have a cell in your spreadsheet always display today's date which function would you use?
(A): TODAY()
(B): DATE()
(C): DATE(today)
(D): SYSTEM(date)
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 30
(Grade style): 0
(Random answers): 1
(Question): All of the following are required to begin creating a PivotTable, except: _____.
(A): Source data must be organized as a list or table
(B): Headings should be in a single row at the top of the source data
(C): Source data must not have empty rows or columns
(D): Begin with an equal sign and open parenthesis
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Intermediate
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 30
(Grade style): 0
(Random answers): 1
(Question): Slicers provide you with _____ for filtering PivotTable data and visualizing what you have filtered.
(A): Charts
(B): Tables
(C): Excel Functions
(D): Buttons
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Excel 2016
(Difficulty): Intermediate
(Applicability): General 
(ENDIGNORE)
