Designing a Presentation - 2 hours
http://www.lynda.com/Keynote-tutorials/Designing-Presentation/124082-2.html
https://www.classmarker.com/a/tests/test/?test_id=484969
Presentation Fundamentals - 1.5 hours
http://www.lynda.com/Business-Skills-tutorials/Presentation-Fundamentals/151544-2.html
https://www.classmarker.com/a/tests/test/?test_id=484971

(Type): multiplechoice
(Category): 17
(Question): Which two practices are most critical in preparing for a presentation? (Choose two)
(A):	Practicing the presentation
(B):	Knowing each of your audience members
(C):	Printing the presentation for your audience
(D):	Using large easy to read text instead of graphics and images
(E):	Researching details of the physical or virtual presentation environment 
(Correct): A, E
(CF): Only A and E are generic best practices that would apply to most presentation situations.

(Type): multiplechoice
(Category): 17
(Question): Which technique represents the best approach to using color schemes in business presentations?
(A):	Limit colors to primary colors only
(B):	Limit colors to web safe colors only
(C):	Use colors that appeal to your audience
(D):	Use a few colors that work well together and use them consistently across slides
(E):	Use a different color on each slide, as long as they come from the same theme / color palette
(Correct): D
(CF): To maximize your audience's ability to focus on your presentation content do not overuse a single color or use too many colors.

(Type): multiplechoice
(Category): 17
(Question): Which two sources are not appropriate to obtain finished graphics for a presentation? (Choose two)
(A):	sxc.hu
(B):	espn.com
(C):	Google images
(D):	thinkstock.com
(E):	gettyimages.com
(Correct): B, C
(CF): Although convenient, Google images and ESPN contain images that are not copyright free.   Since these images are not meant for commercial use they are often poor quality as well.

(Type): multiplechoice
(Category): 17
(Question): Which three fonts would be good choices for normal use in business presentations? (Choose three)
(A):	Calibri
(B):	Helvetica
(C):	Comic Sans
(D):	Harrington
(E):	Times New Roman
(Correct): A, B, E
(CF): Comic Sans and Franklin Gothic are too specialized for normal business use.

(Type): multiplechoice
(Category): 17
(Question): Which two techniques are commonly used to reduce the size of images used in a presentation? (Choose two)
(A):	Convert the image from black and white to color
(B):	Compress the image by converting it to a .jpg format
(C):	Resize the image within the presentation application
(D):	Convert the image to a lossy .bmp format to reduce image size
(E):	Reduce the image resolution using an image editing application
(Correct): B, E
(CF): The correct answers are converting to .jpg format and reducing the image resolution using an image editing application.  The .bmp format is a lossless format that is typically associated with large files.  Converting from black and white to color will typically increase size.  Resizing an image in a presentation application typically scales the image but does not compress it.

(Type): multiplechoice
(Category): 17
(Question): Select three benefits of using a presentation storyboard? (Choose three)
(A):	Avoid work that is unnecessary or redundant
(B):	Help validate and refine your presentation narrative
(C):	Eliminate the need to brainstorm new presentation ideas
(D):	Facilitate the development of key themes into a presentation
(E):	Ensure that a presentation will meet your audience design requirements
(Correct): A, B, D
(CF): Storyboarding is a brainstorming and validation technique used to improve the efficiency of creating a quality presentation.  Although a storyboard is often delivered in the form of visuals designs its main goal is to improve content and flow.

(Type): multiplechoice
(Category): 17
(Question): What is the optimal size for images used in a presentation?
(A):	1080p
(B):	1024x768 or greater for most modern screens or projectors
(C):	Optimal image size (in pixels) should match the screen or projector resolution
(D):	Optimal image size (in pixels) can vary based on image compression level and the image format
(E):	Optimal image size (in pixels) should be calculated based on the screen or projector viewing area they occupy
(Correct): E
(CF): The most efficient image size that balances quality versus size is equal to the number of pixels in the viewing area they occupy.  For example, if your screen resolution is set to 1024 x 768, that is the optimal size for a full-slide image.  If the image occupies only half the width and half the height of the slide, it should be 1024/2 or 512 pixels wide, and 768/2 or 384 pixels high.

(Type): multiplechoice
(Category): 17
(Question): What is the best definition of a presentation focal point?
(A):	The location of the initial content presented on each slide
(B):	Key areas on a slide where audience attention should be directed
(C):	The use of colors and images to subconsciously direct audience attention
(D):	The title region on a slide assuming a traditional left-right/top-bottom presentation approach
(Correct): B
(CF): A focal point is a presentation technique used to direct audience attention to certain places on a screen and control both what they're looking at and what they're perceiving to be the main point.

(Type): multiplechoice
(Category): 17
(Question): Which two are the strongest examples of using a presentation focal point? (Choose two)
(A):	A slide that contains a single quotation centered on the slide
(B):	A slide of monthly user logins containing a ten year bar chart of unique users logins grouped by year
(C):	A slide showing average application performance containing a single scatter plot showing application performance data with outliers highlighted
(D):	A yearly revenue growth slide containing a single graph of yearly revenue for the last ten years with a highlighted trend line
 (Correct): A, D
(CF): A focal point is a presentation technique used to divert audience attention to certain places on a screen and control both what they're looking at and what they're perceiving to be the main point.  Although text focal points are weaker than image focal points Option A is a very simple slide which will easily allow a presenter to control audience focus.  Likewise Option D contains a single graph and highlighted trend line which will also allow a presenter to control audience focus.  The incorrect options either have too much information on the slide or the content of the slide does not match the slide objective.

(Type): multiplechoice
(Category): 17
(Question): Which type of audience is most responsive to a presenter that is unified with his message and has clearly identified next steps?
(A):	Active
(B):	Ready
(C):	Simple
(D):	Skeptical
(E):	Apathetic
(Correct): B
(CF): Option B (Ready Audience) is a type of audience this is open to the message being presented.  This information is directly from the Presentation Fundamentals course.

(Type): multiplechoice
(Category): 17
(Question): Which type of audience is most responsive to a strong presenter that shows concern, identifies the value of their message and is open to questions?
(A):	Active
(B):	Ready
(C):	Simple
(D):	Resistant
(E):	Apathetic
(Correct): D
(CF): Option D (Resistant Audience) is a type of audience this is opposed to the message being presented.  This information is directly from the Presentation Fundamentals course.

(Type): multiplechoice
(Category): 17
(Question): Which are fundamental reasons an audience attends a presentation?
(A):	Technical, Business, Qualitative
(B):	Curiosity, Information, Mandatory
(C):	Development Skills, Testing Skills, Database Skills 
(D):	Topic Relevance, Personal Development, Legal Requirement
(E):	New Ideas, Communication Experience, Challenging Content
(Correct): B
(CF): Option B (Curiosity, Information and Mandatory) is the only option that contains fundamental reasons that are generally applicable to any audience.  This information is directly from the Presentation Fundamentals course.

(Type): multiplechoice
(Category): 17
(Question): What are the three most useful techniques when a presenter is unsure of the audience knowledge level? (Choose three)
(A):	Use acronyms and insider language to maximize value for the audience
(B):	Allocate more time at the end of the presentation for a question and answer period
(C):	Research audience knowledge level and objectives to maximize presentation effectiveness
(D):	Include a lot detail if possible to help novice audience members learn from the presentation
(E):	Start with basic concepts, but ask clarifying questions to tune a presentation to the audience
(Correct): B, C, E
(CF): A presenter must understand his audience and adjust accordingly.  It is not always appropriate to share a lot of detail on a topic especially if audience members are new to the topic.

(Type): multiplechoice
(Category): 17
(Question): Which three attributes are important considerations that drive audience perception of presenter's credibility? (Choose three)
(A):	Slide Design and Content
(B):	Subject Matter Expertise
(C):	Clothing and Appearance
(D):	Tone of Voice and Engagement
(E):	Posture, Body Language, Gestures and Facial Expressions
(Correct): C, D, E
(CF): Much of an audience view of a presenter is determined by non-verbal communication within the first seconds of a presentation or meeting.

(Type): multiplechoice
(Category): 17
(Question): What percentage of message content is communicated between presenter and audience via non-verbal communication?
(A):	About 75%
(B):	About 50%
(C):	About 25%
(D):	Greater than 90%
(Correct): D
(CF): Many studies have shown that most of communication is non-verbal and that first impressions are typically made in the first few seconds of a meeting.

(Type): multiplechoice
(Category): 17
(Question): When does the audience make a judgment on a presenter?
(A):	less than ten seconds
(B):	during the closing slide
(C):	between ten and twenty minutes 
(D):	during the presentation agenda slide
(Correct): A
(CF): Many studies have shown that most communication is non-verbal and that a first impressions is made in the first few seconds of a meeting.

(Type): multiplechoice
(Category): 17
(Grade style): 1
(Question): What are Aristotle's three Pillars of Persuasion (aka Pillars of Public Speaking)?  (Choose three)
(A):	Eros: Sexual appeal
(B):	Logos: Logical appeal
(C):	Creos: Creative appeal
(D):	Ethos: Credibility appeal
(E):	Pathos: Emotional appeal
(Correct): B, D, E
(CF): Aristotle's three Pillars of Persuasion are Logos, Ethos and Pathos.

(Type): multiplechoice
(Category): 17
(Grade style): 1
(Question): What are two questions to ask people that are watching you deliver a final practice run of a very long slide client presentation, just prior to the client meeting? (Choose two)
(A):	Will the flow of the presentation be clear to the client?
(B):	Should I change the presentation slide theme to better represent company branding?
(C):	How could I improve the delivery of Slide #4 to ensure the messages is clear to the client?
(D):	Are there any client project status updates that I should be aware of prior to the meeting?
(E):	Can you send the slide deck to the client to make sure they can access it during the meeting?
(Correct): C, D
(CF): The question describes a final practice run just prior to the meeting.  Only answers C and D are suitable questions at that time.

(Type): multiplechoice
(Category): 17
(Question): Identify three acceptable uses of your hands during a presentation? (Choose three)
(A):	Placed behind your back
(B):	Crossed in front of your body
(C):	Counting numbers on your hands
(D):	At rest with open palms naturally down by your side
(E):	Motioning or pointing to draw audience attention to a relevant bullet on a slide
(Correct): C, D, E
(CF): Arms and hands play an important part in non-verbal communications.  They should be used as appropriate to help illustrate points but a presenter should be careful to ensure they are used in open positions (e.g. open palms) and not closed or awkward positions (e.g. crossed arms, fists, clasped hands). http://www.bakercommunications.com/archive/nov12/presentation110112.html

(Type): multiplechoice
(Category): 17
(Question): Which are the three most critical considerations when presenting to a diverse audience? (Choose three)
(A):	Time of Day
(B):	Native Language
(C):	Knowledge Level
(D):	Cultural Differences
(E):	Font and Image Selection
(Correct): B, C, D
(CF): Answers B, C and D are most likely to impact how a presenter approaches a presentation.  Although answers A and E may be relevant in some situations they are clearly lower priority considerations.

(Type): multiplechoice
(Category): 17
(Question): Which two of the following are opportunities to gain experience delivering a business presentation? (Choose two)
(A):	A wedding toast
(B):	A high school graduation speech
(C):	A presentation to senior management
(D):	A lifetime membership in a Toastmasters International Club
Correct): C, D
(CF): Answers C is correct because as a business presentation it presents the best opportunity for practice.  Answer D is also correct because it offers the opportunity for ongoing practice and exposure to public speaking.

(Type): multiplechoice
(Category): 17
(Question): During a meeting you are called on to provide project status.  This is an example of
(A):	a generic presentation
(B):	an off-script presentation
(C):	a peer review presentation
(D):	an impromptu or adhoc presentation
(Correct): D
(CF): You should always try to apply the best communication and presentation practices to all public speaking even an impromptu status update.

(Type): multiplechoice
(Category): 17
(Question): Which are the two most important reasons to minimize text and images on a slide? (Choose two)
(A):	reduce the cost and time to design and print a presentation
(B):	encourage a presenter to speak naturally and not read the slides
(C):	allow an audience to focus on the presenter and presentation content
(D):	large amounts of content on a slide make it difficult to achieve a balanced slide layout
(Correct): B, C
(CF): Minimizing content on a slide both allows the audience to focus on the presentation and encourages a speaker to talk naturally.

(Type): multiplechoice
(Category): 17
(Question): During a presentation you notice that the audience is starting to lose interest.  Which of the following lines has the best chance of getting their interest back?
(A):	"This is one way we can really wow our clients and shape our industry"
(B):	"With a show of hands how many of you have experienced this same issue"
(C):	"I would greatly appreciate it if everyone could hang with me for a few more minutes"
(D):	"I am sure you all have a lot of suggestions that could improve our applications performance"
(Correct): B
(CF): Option B is the only statement that truly asks for audience participation and it does so in a physical way that engages the entire group.

(Type): multiplechoice
(Category): 17
(Question): You work for an airline corporation that emits high levels of CO2 each year. You are aware of the latest eco-friendly technologies. In a presentation, how can you improve your chances of engaging your CEO?
(A):	Include basic facts and figures to demonstrate expertise
(B):	Ensure you dress appropriately considering your audience
(C):	Make sure your plan is clear using multiple real world examples
(D):	Demonstrate enthusiasm and use stories to trigger an emotional response
(Correct): D
(CF): Many people underestimate the impact of enthusiasm and emotional connection.  Many studies have shown that enthusiasm and passion are more important to influence than expertise or facts and figures.  These skills are characteristic typically associated with high-level leaders.

