(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tim Keyser
(Course Site): lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Just about any project can utilize Agile if deliverables can be produced and implemented in a short period of time, and can be expanded or added to with future capabilities.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Just about any project can utilize Agile if deliverables can be produced and implemented in a short period of time, and can be expanded or added to with future capabilities.
(WF): Just about any project can utilize Agile if deliverables can be produced and implemented in a short period of time, and can be expanded or added to with future capabilities.
(STARTIGNORE)
(Hint):
(Subject): Understand Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): It is not vital for a sponsor to be 100% committed to the Agile process.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): A sponsor needs to be 100% committed to the process. This is vital.
(WF): A sponsor needs to be 100% committed to the process. This is vital.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is a feature?
(A): The issues the team runs into.
(B): A piece of functionality or outcome that is valued by the client.
(C): A piece of functionality or outcome that the team wants to include.
(D): The final product.
(Correct): B
(Points): 1
(CF): A feature is a piece of functionality or outcome that is valued by the client.
(WF): A feature is a piece of functionality or outcome that is valued by the client.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The adapt stage can happen very quickly. How many days does it often get completed in?
(A): 3 days
(B): 2 days
(C): 1 day
(D): 5 days
(Correct): C
(Points): 1
(CF): The Adapt stage can happen very quickly, often being completed in just one day.
(WF): The Adapt stage can happen very quickly, often being completed in just one day.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What should you do if your team gets too big?
(A): Just make it one big team. 
(B): Look into forming multiple Scrum Teams after the current sprint.
(C): Get rid of people.
(D): Ignore the unimportant people.
(Correct): B
(Points): 1
(CF): Larger teams are possible, but it's recommended that you split them into smaller Scrum Teams to stay within the 15 or less guideline. Larger teams introduce risk, as the coordination between larger numbers of people, and the features they produce is much more difficult.
(WF): Larger teams are possible, but it's recommended that you split them into smaller Scrum Teams to stay within the 15 or less guideline. Larger teams introduce risk, as the coordination between larger numbers of people, and the features they produce is much more difficult.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question):  If your iteration is eight weeks total, you should only be spending approximately how many days in the speculate phase?
(A): 5 days
(B): 2 days
(C): 8 days
(D): 10 days
(Correct): A
(Points): 1
(CF):  If your iteration is eight weeks total, you should only be spending approximately five days or so in the speculate phase.
(WF):  If your iteration is eight weeks total, you should only be spending approximately five days or so in the speculate phase.
(STARTIGNORE)
(Hint):
(Subject): Envisioning
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What do you use to determine if open issues are growing in volume?
(A): Backlog
(B): Issues Register
(C): Stories
(D): Management
(Correct): B
(Points): 1
(CF): Your issues register will provide the mechanism to determine if open issues are growing in volume. This could be an indication that something is wrong and changes should be considered.
(WF): Your issues register will provide the mechanism to determine if open issues are growing in volume. This could be an indication that something is wrong and changes should be considered.
(STARTIGNORE)
(Hint):
(Subject): Speculating
(Difficulty): Beginner
(Applicability): Course  
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): On a weekly basis, you may want to invite additional project stakeholders to the meeting.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): On a weekly basis, you may want to invite additional project stakeholders to the meeting.
(WF): On a weekly basis, you may want to invite additional project stakeholders to the meeting.
(STARTIGNORE)
(Hint):
(Subject): Speculating
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The role as a project manager is to observe and guide versus lead.
(A): True
(B): False
(Correct): True
(Points): 1
(CF): Your role as a project manager is to observe and guide versus lead. You actually take a back seat to the process and you lead via coaching.
(WF): Your role as a project manager is to observe and guide versus lead. You actually take a back seat to the process and you lead via coaching.
(STARTIGNORE)
(Hint):
(Subject): Managing the Building Process
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): One of the best characteristics of agile project management is the opportunity to obtain feedback frequently and apply changes based on what the team has learned.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): One of the best characteristics of agile project management is the opportunity to obtain feedback frequently and apply changes based on what the team has learned.
(WF): One of the best characteristics of agile project management is the opportunity to obtain feedback frequently and apply changes based on what the team has learned.
(STARTIGNORE)
(Hint):
(Subject): Adapting and Closing
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)