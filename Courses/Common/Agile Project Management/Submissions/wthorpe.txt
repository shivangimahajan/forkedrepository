(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Wthorpe
(Course Site): lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of the following items are not unique to Agile, nor strongly emphasized by agile?
(A): Face to face communication is emphasized over documentation
(B): Teams are collocated to be together
(C): Requirement changes are anticipated and accommodated
(D): Requirements must be understood
(E): Use a shared and managed schedule
(Correct): D,E
(Points): 1
(CF): A shared and managed schedule is important for any project type.  Requirements are even more important in a waterfall style development.
(WF): A shared and managed schedule is important for any project type.  Requirements are even more important in a waterfall style development.
(STARTIGNORE)
(Hint):
(Subject): Agile
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which item is NOT on the project charter?
(A): Target customer
(B): Key benefits
(C): Project sponsor 
(D): Development team members
(Correct): D
(Points): 1
(CF): The charter is a higher level document, specifics of things like team members or resources would be found in the Product Data Sheet.
(WF): The charter is a higher level document, specifics of things like team members or resources would be found in the Product Data Sheet.
(STARTIGNORE)
(Hint):
(Subject): Agile Envision
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are some common examples of team norms to prepare before an agile project? (check all that apply)
(A): Where the team is physically located and how they work together
(B): What hours the members will work
(C): Instituting a rule that states team members should report issues they see and suggest resolutions to them
(D): Always Avoid risk
(E): Ignore conflicts with others to focus on project
(Correct): A,B,C
(Points): 1
(CF): Risk needs to be taken, and conflicts should be resolved, just not in daily meetings.
(WF): Risk needs to be taken, and conflicts should be resolved, just not in daily meetings.
(STARTIGNORE)
(Hint):
(Subject): Agile Envision
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are some risks to consider with an agile project?
(A): Inexperienced members have trouble with processes
(B): The sprint schedule is overly ambitious
(C): Team members are not willing to take risks and make decisions without approval
(D): Management will not be satisfied by half finished initial releases
(Correct): A,B,C,D
(Points): 1
(CF): Process inexperience in both the Scrum Team and the Business can be mitigated with less ambitious early sprints.
(WF): Process inexperience in both the Scrum Team and the Business can be mitigated with less ambitious early sprints.
(STARTIGNORE)
(Hint):
(Subject): Agile
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are some important adjustments that can come out of the adapt phase?
(A): Adding or removing features
(B): Re-prioritizing features
(C): Changing team members
(D): Changing the product owner
(E): Updating the project budget
(Correct): A,B,C
(Points): 1
(CF): The product owner and budget tend to be locked in pretty solidly during the envision stage.
(WF): The product owner and budget tend to be locked in pretty solidly during the envision stage.
(STARTIGNORE)
(Hint):
(Subject): Agile Adapt
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of these qualities tends to indicate a project that would be a good candidate for Agile?
(A): Expect requirements to change or evolve
(B): The product can deliver business value incrementally
(C): Done on a regular basis with a proven set of steps
(D): There is no business value in anything but the finished project deliverable
(E): The requirements are very strongly set, and unlikely to change
(Correct): A,B
(Points): 1
(CF): unvarying requirements and a proven process tend to be opposite the benefits of the Agile process.
(WF): unvarying requirements and a proven process tend to be opposite the benefits of the Agile process.
(STARTIGNORE)
(Hint):
(Subject): Agile
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which information are you unlikely to find on a project data sheet?
(A): Project description and objective
(B): Cost estimates (and people needed)
(C): Constraints and limitations
(D): List of User stories
(Correct): D
(Points): 1
(CF): User stories are a tool used further down the line.  the data sheet has a wider focus on what is needed for the project.
(WF): User stories are a tool used further down the line.  the data sheet has a wider focus on what is needed for the project.
(STARTIGNORE)
(Hint):
(Subject): Agile Envision
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of the following constraints might be seen on a PDS constraints list?
(A): Environmental, such as location
(B): Safety, such as official standards
(C): Cost, such as the total cost available over the course of the project
(D): Times, such as a specific required end date.
(Correct): A,B,C,D
(Points): 1
(CF): Constraints can contain almost any type of requirement or limitation at a project level, these are all correct.
(WF): Constraints can contain almost any type of requirement or limitation at a project level, these are all correct.
(STARTIGNORE)
(Hint):
(Subject): Agile Envision
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which use case example summary is not valid?
(A): User interacts with atm machine to check account balance
(B): System administrator accesses settings in online tool to update user permissions
(C): System administrator explains online tool to user about password requirements
(D): System monitor process sends email to user about error condition
(Correct): C
(Points): 1
(CF): The administrator is not interacting with another actor rather than a system. A system is the primary point of the use case.
(WF): The administrator is not interacting with another actor rather than a system. A system is the primary point of the use case.
(STARTIGNORE)
(Hint):
(Subject): Agile Speculate
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of these statements are correct for the 'fist of five' voting system?
(A): Each person is given 5 votes to hold on to for use during the meeting
(B): 5 team members should play rock paper scissors to determine scrum lead
(C): Each team member should hold up a hand with a number of fingers up to vote
(D): Subdividing larger project teams into subgroup of 5 can help improve sprint velocity
(Correct): C
(Points): 1
(CF): Fist of five is a voting system with an added level of detail over just a show of hands, allowing more detail on votes.
(WF): Fist of five is a voting system with an added level of detail over just a show of hands, allowing more detail on votes.
(STARTIGNORE)
(Hint):
(Subject): Agile Adapt
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


