(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tsetsen erdene Ganbaatar
(Course Site): lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of these columns should exist on a standard Scrum Task board? (Select all that apply)
(A): Sprint Backlog
(B): Dev In Progress
(C): Favorite Sprints
(D): Done
(E): Deprecated
(Correct): A,B,D
(Points): 1
(CF): The course lists: Sprint Backlog, To Do, Dev In Progress and Done.
(WF): The course lists: Sprint Backlog, To Do, Dev In Progress and Done.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): All legacy non-agile documents should be retrofitted when transitioning to Agile.
(A):True
(B):False
(Correct): B
(Points): 1
(CF): Very few (if any) documents should be retrofitted to agile as they often reduce the agility of a team.
(WF): Very few (if any) documents should be retrofitted to agile as they often reduce the agility of a team.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): For whom is the burndown chart made and updated for?
(A): All of these apply
(B): Development Team
(C): Scrum Master
(D): Product Owner
(E): Important Stakeholders
(Correct): A
(Points): 1
(CF): The burndown chart is useful for anyone interested in the project's progress.
(WF): The burndown chart is useful for anyone interested in the project's progress.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of these agile phases should only be done once?
(A): Speculation
(B): Exploration
(C): Construction
(D): Close
(E): Testing
(Correct): D
(Points): 1
(CF): All phases other than the Envision and Close phases are iterated.
(WF): All phases other than the Envision and Close phases are iterated.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Agile is a good methodology to pick when the requirements are well known ahead of time.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): If the requirements are well known, defined and don't change often, then agile is overkill and wastes time.
(WF): If the requirements are well known, defined and don't change often, then agile is overkill and wastes time.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
