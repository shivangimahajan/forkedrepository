
(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jacob Lista
(Course Site): lynda
(Course Name): SQL Essential Training
(Course URL): https://www.lynda.com/SQL-tutorials/Goodbye/139988/166317-4.html
(Discipline): Professional
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 0
(Question): True or False: The commands learned from the SQL training videos can always be used in whichever database management system you might be using.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Different implementations may have different rules and different commands.
(WF): Different implementations may have different rules and different commands.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Which of the following is correct?
(A): NULL is a value like any other and can be compared with values using the = operator.
(B): NULL is a value, but can't be compared with other values using the = operator.
(C): NULL is not a value, but it can be compared with values using the = operator.
(D): NULL is not a value, and it can't be compared with values using the = operator.
(Correct): D
(Points): 1
(CF): NULL is the absence of a value.
(WF): NULL is the absence of a value.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What is the primary reason to use good formatting practices in your SQL code?
(A): It improves readability
(B): Formatting choices like spaces and line breaks affect the operation of the code
(C): Formatting does not matter and it is not suggested to use any formatting practices
(D): SQL code formatted in different ways will run differently depending on the database management system
(Correct): A
(Points): 1
(CF): Formatting does not affect the code's operation.
(WF): Formatting does not affect the code's operation.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

