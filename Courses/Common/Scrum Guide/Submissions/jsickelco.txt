(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jeff Sickelco
(Course Site): Scrum.org
(Course Name): Scrum Guide
(Course URL): https://www.scrum.org/Portals/0/Documents/Scrum%20Guides/2013/Scrum-Guide.pdf
(Discipline): Professional 
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who bears ultimate responsibility for work estimates?
(A): The product owner
(B): The development team
(C): The Scrum Master
(D): Business analysts
(E): The scrum team
(Correct): B
(Points): 1
(CF): The development team, which consists only of developers, is responsible for estimating the work they will complete.
(WF): The development team, which consists only of developers, is responsible for estimating the work they will complete.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5 
(Grade style): 1
(Random answers): 1
(Question): Which of the following describes the development team?
(A): It encompasses people with the following titles: tester, analyst, developer
(B): It is self-organizing
(C): It is cross-functional
(D): It is divided into two sub-teams
(E): Their work creates the releasable "Done" increment at the end of each sprint
(Correct): B, C, E
(Points): 1
(CF): Development teams have no sub-groups and all members have the title of "developer." They are cross-functional, self-organizing, and responsible for carrying out the work necessary to release an increment each sprint.
(WF): Development teams have no sub-groups and all members have the title of "developer." They are cross-functional, self-organizing, and responsible for carrying out the work necessary to release an increment each sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): For a one-month sprint, roughly how much time should be allotted for sprint planning, sprint review, and sprint retrospectives?
(A): 8 hours for sprint planning, 4 hours for sprint review, and 3 hours for sprint retrospective
(B): 1 hour for sprint planning, 1 hour for sprint review, and 2 days for sprint retrospective
(C): 1 week for sprint planning, 1 week for sprint review, and 2 days for sprint retrospective
(D): 1 week for sprint planning, 4 hours for sprint review, and 3 hours for sprint retrospective
(E): 2 hours for sprint planning, 2 hours for sprint review, and 3 hours for sprint retrospective
(Correct): A
(Points): 1
(CF): For a one-month sprint, a team should allot 8 hours for sprint planning, 4 hours for sprint review, and 3 hours for a sprint retrospective.
(WF): For a one-month sprint, a team should allot 8 hours for sprint planning, 4 hours for sprint review, and 3 hours for a sprint retrospective.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which does NOT describe the Product Backlog?
(A): It is always changing
(B): It exists as long as the product exists
(C): It is an ordered list of everything that might be needed in the project
(D): It represents the work that will be completed during a single sprint
(E): It's items have a description, order, estimate, and value
(Correct): D
(Points): 1
(CF): The Product Backlog represents all work needed for a project, not work limited to a single sprint.
(WF): The Product Backlog represents all work needed for a project, not work limited to a single sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who is responsible for maintaining the Product Backlog?
(A): The product owner
(B): The team lead
(C): The business analyst
(D): The project stakeholder
(E): The backlog master
(Correct): A
(Points): 1
(CF): The product owner maintains the product backlog.
(WF): The product owner maintains the product backlog.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is the Increment?
(A): The sum of all product backlog items completed in a sprint and the value of all previous sprint increments
(B): The amount of time each sprint takes
(C): The sum of all developer work-hours during a sprint
(D): The work remaining in a sprint
(E): The total work remaining in the product backlog
(Correct): A
(Points): 1
(CF): The increment is the sum of all product backlog items completed in a sprint and the value of all previous sprint increments.
(WF): The increment is the sum of all product backlog items completed in a sprint and the value of all previous sprint increments.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following does NOT describe the sprint review?
(A): Key stakeholders may be invited
(B): The development team demonstrates the work it completed during the sprint
(C): The product owner describes what elements of product backlog have been completed
(D): The development team discusses what went well during the sprint
(E): It is a formal meeting
(Correct): E
(Points): 1
(CF): The sprint review is an informal meeting.
(WF): The sprint review is an informal meeting.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What of the following is a responsibility of the Scrum Master?
(A): Maintaining the product backlog
(B): Ensuring Scrum events occur and time-boxes are respected
(C): Coding the product
(D): Estimating tasks
(E): Analyzing business requirements
(Correct): B
(Points): 1
(CF): The Scrum Master is responsible for ensuring Scrum events occur and do not exceed the appropriate duration.
(WF): The Scrum Master is responsible for ensuring Scrum events occur and do not exceed the appropriate duration.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Why might a sprint be canceled?
(A): Market forces make the focus of the sprint obsolete
(B): The time needed to complete the sprint backlog exceeds the duration of the sprint
(C): The product backlog needs to be refined
(D): An unexpected number of bugs forces a development halt
(E): Canceling a sprint is impossible
(Correct): A
(Points): 1
(CF): A sprint is only canceled under unusual circumstances, such as outside market forces rendering the intended outcome of a sprint obsolete.
(WF): A sprint is only canceled under unusual circumstances, such as outside market forces rendering the intended outcome of a sprint obsolete.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): When should the development team deliver a working product?
(A): Mid-way through the project lifespan
(B): At the sprint review for the project's final sprint
(C): Sometime in the last two sprints
(D): Whenever the project stakeholders demand it
(E): After every sprint
(Correct): E
(Points): 1
(CF): Due to the scrum method's iterative and incremental nature, some form of a working product is available after each sprint.
(WF): Due to the scrum method's iterative and incremental nature, some form of a working product is available after each sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)