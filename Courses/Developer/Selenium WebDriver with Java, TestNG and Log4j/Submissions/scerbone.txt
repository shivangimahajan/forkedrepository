(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): udemy.com
(Course Name): Selenium WebDriver with Java, TestNG and Log4j
(Course URL): https://www.udemy.com/selenium-webdriver-with-java-testng-and-log4j/?dtcode=KY9eCax2Pk2j#/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): How would you find a link with an id of 'someId' with a relative XPath?
(A): //a[@id='someId']
(B): //[type='a', id='someId']
(C): //[a, @id='someId']
(D): //a[id='someId']
(E): //a[link='someId']
(Correct): A
(Points): 1
(CF): The correct format of a relative XPath with an id of 'someId' is: //a[@id='someId'].
(WF): The correct format of a relative XPath with an id of 'someId' is: //a[@id='someId'].
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): How do you load a url with WebDriver? myUrl = "https://www.google.com"
(A): WebDriver.get(myUrl);
(B): WebDriver.load(myUrl);
(C): driver.get(myUrl);
(D): driver.load(myUrl);
(E): WebDriver.goTo(myUrl);
(Correct): C
(Points): 1
(CF): The correct way to load a WebDriver url is: driver.get(myUrl);
(WF): The correct way to load a WebDriver url is: driver.get(myUrl);
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):18
(Grade style): 0
(Random answers): 1
(Question): You can find an element in WebDriver with its id ("someId") using: driver.findElement(By.id("someId"));
(A): True
(B): False
(Correct): A
(Points): 1
(CF): You can find an element in WebDriver with its id ("someId") using: driver.findElement(By.id("someId"));
(WF): You can find an element in WebDriver with its id ("someId") using: driver.findElement(By.id("someId"));
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): WebDriver has no built in drag and drop method, but it is possible to implement drag and drop using the "moveToElement" method.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): WebDriver has a built in "dragAndDrop" method, but it is also possible to implement drag and drop using the "moveToElement" method.
(WF): WebDriver has a built in "dragAndDrop" method, but it is also possible to implement drag and drop using the "moveToElement" method.
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What is Log4j?
(A): A logging and compiling utility.
(B): A logging and optimization utility.
(C): Strictly a logging utility
(Correct): C
(Points): 1
(CF): Log4j is strictly a logging utility.
(WF): Log4j is strictly a logging utility.
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Each test method in TestNG can only be assigned to a single test group.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Each test method in TestNG can be used in multiple test groups.
(WF): Each test method in TestNG can be used in multiple test groups.
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): In TestNG what is the name of a function that provides parameters for a test method?
(A): ParameterProvider
(B): ParameterOutput
(C): VariableProvider
(D): DataOutput
(E): DataProvider
(Correct): E
(Points): 1
(CF): The correct name of a function that provides parameters for a test method is a DataProvider.
(WF): The correct name of a function that provides parameters for a test method is a DataProvider.
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What is the correct annotation for a test case that depends on another test case called "someOtherMethod"?
(A): @Test(dependsOnMethods={"someOtherMethod"})
(B): @Dependencies(dependsOnMethods={"someOtherMethod"})
(C): @Test(dependsOnMethod={"someOtherMethod"})
(D): @Test(dependencies={"someOtherMethod"})
(E): @Test(dependsOnMethods="someOtherMethod")
(Correct): A
(Points): 1
(CF): The correct annotation would be: @Test(dependsOnMethods={"someOtherMethod"}).
(WF): The correct annotation would be: @Test(dependsOnMethods={"someOtherMethod"}).
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): By changing the line "driver = new FirefoxDriver();" to "driver = new ChromeDriver();", assuming all dependencies are imported, the test case would run in chrome.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): This change would not be enough. You must also set the property of "webdriver.chrome.driver" to the path of the downloaded "chromedriver.exe".
(WF): This change would not be enough. You must also set the property of "webdriver.chrome.driver" to the path of the downloaded "chromedriver.exe".
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Listeners can only be defined in the TestNG .xml file.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Listeners can be defined in the TestNG .xml file and as an annotation above a test case method. As such: @Listeners([packageName].[listenerclassname].class)
(WF): Listeners can be defined in the TestNG .xml file and as an annotation above a test case method. As such: @Listeners([packageName].[listenerclassname].class)
(STARTIGNORE)
(Hint):
(Subject): Selenium WebDriver with Java, TestNG and Log4j
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)