(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ziyan Wang
(Course Site): Code School
(Course Name): Front-end Formations
(Course URL): https://www.codeschool.com/courses/front-end-formations/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a HTML5 semantic element?
(A): section
(B): header
(C): footer 
(D): nav
(E): ul
(Correct): E 
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTML5
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Which of the following are HTML5 new input types? (choose three)
(A): range
(B): email
(C): search 
(D): password
(E): checkbox
(Correct): A,B,C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTML5
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What exclusive feature can the <datalist> element provide?
(A): Autocomplete on input fields
(B): Add a dropdown menu
(C): Output a list of radio buttons with labels
(D): Realtime form validation 
(Correct): A
(Points): 1
(CF): <datalist> element can be used to provide autocomplete feature as user input data
(WF): <datalist> element can be used to provide autocomplete feature as user input data
(STARTIGNORE)
(Hint):
(Subject): HTML5
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What is true for blur radius and spread radius in box-shadow property? (choose two)
(A): Blur radius controls how blurry the shadow will be
(B): Spread radius controls the width of the shadow 
(C): Blur radius and spread radius can not be applied at the same time
(D): Neither blur radius nor spread radius can have a value of 0px 
(Correct): A,B
(Points): 1
(CF): Blur radius controls how blurry the shadow will be, and spread radius controls the width of the shadow 
(WF): Blur radius controls how blurry the shadow will be, and spread radius controls the width of the shadow 
(STARTIGNORE)
(Hint):
(Subject): CSS3
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Which CSS3 property should be used to draw a circle?
(A): border-radius
(B): margin
(C): padding
(D): position
(Correct): A
(Points): 1
(CF): border-radius should be used to draw a circle
(WF): border-radius should be used to draw a circle
(STARTIGNORE)
(Hint):
(Subject): CSS3
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Which is correct if box-sizing property is set to content-box?
(A): The width and height property includes only the content
(B): The width and height property includes the content, border, padding, and margin
(C): The width and height property includes only border, padding, and margin
(D): The width and height property includes only the content and padding
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): CSS3
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Which of the following is a valid value for the opacity property?
(A): 50%
(B): 0.6
(C): 3
(D): 10px
(Correct): B
(Points): 1
(CF): Opacity value ranges from 0 to 1
(WF): Opacity value ranges from 0 to 1
(STARTIGNORE)
(Hint):
(Subject): CSS3
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What does the property transition-timing-function do?
(A): It makes the transition effect to change speed over its duration
(B): It makes the transition effect to occur after a certain amount of delay
(C): It limits the amount of time for the transition effect to complete
(D): It keeps track of the duration for a transition effect 
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): CSS3
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What is CSS box model?
(A): It defines different types of positions for each HTML element
(B): Every HTML element is implemented as a box with height and width properties
(C): Every HTML element is wrapped in a box, which consists of margin, border, padding, and the content
(D): It defines the document flow within a page
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): CSS3
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Which is correct about HTML5 <main> tag? (choose two)
(A): The <main> tag specifies the main content of a document
(B): The content inside the <main> element should be unique to the document
(C): It is a good practice to have more than one <main> element in a document
(D): It is recommended to place <main> element within <header> element
(Correct): A,B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTML5
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
