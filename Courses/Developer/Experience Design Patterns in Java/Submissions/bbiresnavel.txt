(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brandon Bires-Navel
(Course Site): Udemy
(Course Name): Experience Design Patterns in Java
(Course URL): https://www.udemy.com/experience-design-patterns/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 14
(Grade style): 0
(Random answers): 1
(Question): The overall goals of design patterns are to: (pick two)
(A): Solve common problems when designing an application or system
(B): To make software engineering more difficult and strenuous 
(C): Reduce coupling and increase cohesion
(D): Give the Java programming language extra tools for solving problems
(Correct): A,C
(Points): 1
(CF): Design patterns describe an abstract way to solve a application or system problem and are not specific to any one programming language.
(WF): Design patterns describe an abstract way to solve a application or system problem and are not specific to any one programming language.
(STARTIGNORE)
(Hint):
(Subject): Java Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 14
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not a creational design pattern?
(A): Singleton pattern
(B): Builder pattern
(C): Composite pattern
(D): Abstract factory
(E): Prototype pattern
(Correct): C
(Points): 1
(CF): Design patterns are generally split into three categories -- creational, structural, and behavioral.
(WF): Design patterns are generally split into three categories -- creational, structural, and behavioral.
(STARTIGNORE)
(Hint):
(Subject): Java Design Patterns
(Difficulty): Easy
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 14
(Grade style): 0
(Random answers): 1
(Question): The Façade pattern is a design pattern that
(A): ensures a class only has one instance, and provides a global point of access to it
(B): makes the program faster for the end user
(C): decouples an abstraction from its implementation so that the two can vary independently
(D): provides a unified interface to a set of interfaces in a subsystem
(Correct): D
(Points): 1
(CF): The Façade pattern is useful for abstracting the inner-workings of a complex system.
(WF): The Façade pattern is useful for abstracting the inner-workings of a complex system.
(STARTIGNORE)
(Hint):
(Subject): Java Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
