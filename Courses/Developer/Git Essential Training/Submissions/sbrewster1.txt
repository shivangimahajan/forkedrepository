(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):
(Course Site):
(Course Name):
(Course URL):
(Discipline):
(ENDIGNORE)

(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): After using 'git add .' to stage multiple files, you realize you don't want to commit 'myfile.txt' just yet. What command do you use?
(A): git reset --staged
(B): git reset --hard
(C): git reset HEAD myfile.txt
(D): git reset HEAD
(E): git unstage myfile.txt
(Correct): C
(Points): 1
(CF): Correct!
(WF): See Part 5 - Adding files
(STARTIGNORE)
(Hint):
(Subject): Git	
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse 
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): The .gitignore file allows for some basic regular expression symbols such as: * ? [aeiou] [0-9] !
(A): True
(B): False 
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 8 - Using .gitignore files
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 1
(Random answers): 1
(Question): A tree-ish identifier can be...
(A): Part or full SHA-1 hash
(B): HEAD pointer
(C): Branch reference
(D): Tag reference
(E): Commit log message
(Correct): A,B,C,D
(Points): 1
(CF): Correct!
(WF): See Part 9 - Referencing commits
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which of these identifier pairs point to the same commit?
(A): HEAD^^, HEAD~1
(B): feature_branch^^^, feature_branch~3
(C): acf87504^, dea87901^
(D): HEAD, HEAD~
(Correct): B
(Points): 1
(CF): Correct!
(WF): See Part 9 - Referencing commits
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 1
(Random answers): 1
(Question): Select all valid Git log commands
(A): git log --since=2.weeks --until=2.days
(B): git log --author="John Doe"
(C): git log --grep="t23401"
(D): git log --oneline -3
(E): git log --verbose
(Correct): A,B,C,D
(Points): 1
(CF): Correct!
(WF): See Part 9 - Getting more from the commit log
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): The SHA-1 hash identifier for the HEAD is located in .git/HEAD.
(A): True
(B): False 
(Correct): B 
(Points): 1
(CF): Correct!
(WF): .git/HEAD contains a path to the SHA-1 hash located in refs/heads/<branch>
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 1
(Random answers): 1
(Question): To view the listing for a tree, use the command... 
(A): git ls-tree <tree-ish>
(B): git ls-tree <SHA-1>
(C): git show <tree-ish>
(D): git show <SHA-1 id>
(Correct): A,B
(Points): 1
(CF): Correct!
(WF): 'git show' is for viewing specific commits. See Part 9 - Exploring tree listings
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): To see all branches that contain everything which is also contained in the current branch, use 'git branch' plus which option?
(A): --merged
(B): --contains-all
(C): --no-diff
(D): --subset
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 10 - Comparing branches
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): If you just merged two branches but there are now conflicts and you no longer want to continue with the merge, which commmand should you use?
(A): git merge --undo
(B): git unmerge
(C): git merge --abort
(D): git unmerge --hard
(E): git merge --cancel
(Correct): C
(Points): 1
(CF): Correct!
(WF): See Part 11 - Resolving merge conflicts
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): There are two ways to delete a remote branch: 'git push origin :branch_name' and 'git push origin --delete branch_name'.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): Correct!
(WF): See Part 13 - Deleting a remote branch
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)