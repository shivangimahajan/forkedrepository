

(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jacob Lista
(Course Site): lynda
(Course Name): GIT Essential Training
(Course URL): https://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:GIT%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Professional
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 0
(Question): True or False: In a project with multiple repositories, Git designates a master repository.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Designating a master repository is a user convention and is not built into Git.
(WF): Designating a master repository is a user convention and is not built into Git.
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which of the following is/are correct?
(A): Git is ideal for tracking changes in files which are associated with a specific program (like .doc or .pdf).
(B): A user of Git needs to be connected to the network to work on a distributed project.
(C): Git is effective both for tracking changes to personal projects and for collaborating with others.
(D): All of the above.
(Correct): C
(Points): 1
(CF): Git is mainly useful for working with text or source code files, and a user need not be connected to a network to use it.
(WF): Git is mainly useful for working with text or source code files, and a user need not be connected to a network to use it.
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Why is it suggested to create a branch when trying out a new feature?
(A): Git will not allow you to add new features without first creating a new branch.
(B): It is easy to go back to the master branch if the new feature doesn't work out.
(C): You should not put the new features in a branch. Instead, you should put them in a stash.
(D): A branch can be easily accessed by other users who want to try the feature, while the main repository cannot.
(Correct): B
(Points): 1
(CF): Git's branching feature allows users to decide which changes to merge into their own repositories, and branches are easy to create and delete.
(WF): Git's branching feature allows users to decide which changes to merge into their own repositories, and branches are easy to create and delete.
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
