(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jeff Sickelco
(Course Site): www.lynda.com
(Course Name): Git Essential Training
(Course URL) : http://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:GIT%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Technical 
(ENDIGNORE)

(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): What does HEAD^ represent in Git?
(A): The most recent commit
(B): A branch called HEAD
(C): The initial commit
(D): The parent of the most recent commit
(E): A stash
(Correct): D
(Points): 1
(CF): HEAD references the most recent commit, and the '^' symbol indicates the parent, so HEAD^ is the parent of the most recent commit.
(WF): HEAD references the most recent commit, and the '^' symbol indicates the parent, so HEAD^ is the parent of the most recent commit.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13 
(Grade style): 0
(Random answers): 1
(Question): When using a common command like "git show," how many digits of the commit SHA value do you need to provide?
(A): 10
(B): All of them
(C): 1
(D): 4
(E): Just enough to be unique, usually 8-10 for a midsize project
(Correct): E
(Points): 1
(CF): You only need to provide enough digits of the SHA for Git to be able to determine a unique commit; there is no fixed number of digits required.
(WF): You only need to provide enough digits of the SHA for Git to be able to determine a unique commit; there is no fixed number of digits required.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): What happens if you fail to provide a commit message when using the "git commit" command?
(A): You will receive an error from Git
(B): You will be allowed to check-in, but it will be difficult for others to tell what your changeset accomplished
(C): Git will generate a default commit message for you
(D): Git will open a text editor in which to type your commit message
(E): Git will generate a SHA-1 value to use as the message
(Correct): D
(Points): 1
(CF): If you don't provide an inline commit message on the command line, Git will open a text editor in which you may write your message.
(WF): If you don't provide an inline commit message on the command line, Git will open a text editor in which you may write your message.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): What are the components of Git's three-tree architecture?
(A): Local, Origin, and Remote
(B): Working Copy, Staging Index, and Repository
(C): Branch, Commit, Merge
(D): Hard, Soft, Mixed
(E): Local, Staging, Final
(Correct): B
(Points): 1
(CF): The components of the three-tree architecture are the working copy, the staging index, and the repository.
(WF): The components of the three-tree architecture are the working copy, the staging index, and the repository.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which of the following commands would rename a file?
(A): git mv original_filename.txt new_filename.txt
(B): git rename original_filename.txt new_filename.txt
(C): git rn original_filename.txt new_filename.txt
(D): git swap original_filename.txt new_filename.txt
(E): This operation cannot be done on the command line, but if you change the filename in the OS file browser, Git will recognize the change
(Correct): A
(Points): 1
(CF): A move and a rename are the same thing in Git, so you would use the 'mv' command to rename a file.
(WF): A move and a rename are the same thing in Git, so you would use the 'mv' command to rename a file.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which of the following is true regarding amending commits?
(A): You can only amend the most recent commit
(B): You can only amend a commit that is less than an hour old
(C): You can only amend a commit message, not the contents of the commit
(D): If you amend a commit, Git will re-calculate all the SHA values back through the commit ancestry
(E): You cannot amend a commit
(Correct): A
(Points): 1
(CF): You can only amend the most recent commit.
(WF): You can only amend the most recent commit.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 1
(Random answers): 1
(Question): Which of the following describe ways to modify project-level configuration settings in Git?
(A): Manually edit the config file inside the .git folder within your project
(B): Use the "git config --global" command
(C): Use the "git config" command
(D): Manually edit the "gitconfig" file inside "Program Files/Git/etc/" (on windows) or "/etc/" (on Unix)
(E): Use the "git alter" command
(Correct): A, C
(Points): 1
(CF): To edit project-level configuration in Git, either edit the config file within the project's .git folder or, preferably, use the "git config" command without modifiers.
(WF): To edit project-level configuration in Git, either edit the config file within the project's .git folder or, preferably, use the "git config" command without modifiers.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 1
(Random answers): 1
(Question): Which of the following types of files should you set up to be ignored based on settings in .gitignore?
(A): Text source code files
(B): Compiled source code
(C): PHP files
(D): OS-generated files
(E): Logs
(Correct): B, D, E
(Points): 1
(CF): Files such as compiled source code, OS-generated files, and logs should be ignored, as they are not directly related to your project and are likely to undergo frequent, incidental changes.
(WF): Files such as compiled source code, OS-generated files, and logs should be ignored, as they are not directly related to your project and are likely to undergo frequent, incidental changes.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which of the following Git commands enables you to see an abbreviated, readable version of the commit history?
(A): git log --oneline
(B): git log --grep "summary"
(C): git show HEAD
(D): git history
(E): git diff -b
(Correct): A
(Points): 1
(CF): "git log --oneline" will allow you to view the commit log as a series of one-line entries.
(WF): "git log --oneline" will allow you to view the commit log as a series of one-line entries.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which of the following describes a fast-forward merge?
(A): A merge which Git can execute simply by moving the HEAD pointer, without actually doing a commit
(B): A merge which uses compressed files for speed
(C): A merge which requires manual conflict resolution
(D): A merge for which a commit will be created
(E): A merge from a feature branch back into the main or trunk branch
(Correct): A
(Points): 1
(CF): In a fast-forward merge, Git simply moves the HEAD pointer.
(WF): In a fast-forward merge, Git simply moves the HEAD pointer.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)